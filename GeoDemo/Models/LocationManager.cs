﻿using System;
using CoreLocation;
using GeoDemo.Core.Models;
using UIKit;

namespace GeoDemo.Models
{
    public class LocationManager
    {
        protected CLLocationManager locMgr;

        public event EventHandler<RegionEventArgs> RegionEntered = delegate { };
        public event EventHandler<RegionEventArgs> RegionLeft = delegate { };
        public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };

        public LocationManager()
        {
            this.locMgr = new CLLocationManager
            {
                PausesLocationUpdatesAutomatically = false
            };

            // iOS 8 has additional permissions requirements
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                locMgr.RequestAlwaysAuthorization();
            }

            if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
            {
                locMgr.AllowsBackgroundLocationUpdates = true;
            }
        }

        public CLLocationManager LocMgr
        {
            get { return this.locMgr; }
        }

        public bool StartLocationUpdates()
        {
            if (CLLocationManager.LocationServicesEnabled
                && CLLocationManager.Status != CLAuthorizationStatus.Denied)
            {
                LocMgr.DesiredAccuracy = 10;

                LocMgr.RegionEntered += (object sender, CLRegionEventArgs e) =>
                {
                    RegionEntered(this, new RegionEventArgs(e.Region));
                };

                LocMgr.RegionLeft += (object sender, CLRegionEventArgs e) =>
                {
                    RegionLeft(this, new RegionEventArgs(e.Region));
                };

                locMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
                {
                    LocationUpdated(this, new LocationUpdatedEventArgs(e.Locations[e.Locations.Length - 1]));
                };

                LocMgr.StartUpdatingLocation();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddRegionToMonitor(GeoItemLocation geoItemLocation)
        {
            var region = new CLCircularRegion(new CLLocationCoordinate2D(geoItemLocation.Address.Latitude, geoItemLocation.Address.Longitude), 50, geoItemLocation.Name);

            if (CLLocationManager.IsMonitoringAvailable(typeof(CLCircularRegion)))
            {
                LocMgr.StartMonitoring(region);
                return true;
            }

            return false;
        }
    }
}