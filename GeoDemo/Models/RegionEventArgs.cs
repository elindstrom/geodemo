﻿using System;
using CoreLocation;

namespace GeoDemo.Models
{
    public class RegionEventArgs : EventArgs
    {
        CLRegion region;

        public RegionEventArgs(CLRegion region)
        {
            this.region = region;
        }

        public CLRegion Region
        {
            get { return region; }
        }
    }
}
