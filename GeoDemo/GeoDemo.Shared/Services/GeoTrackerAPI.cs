﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeoDemo.Shared.Models;

namespace GeoDemo.Shared.Services
{
    public class GeoTrackerAPI : IGeoTrackerAPI
    {
        public List<GeoEvent> Events { get; set; }
        public List<GeoItemLocation> Locations { get; set; }

        public GeoTrackerAPI()
        {
            Events = new List<GeoEvent>();
            Locations = new List<GeoItemLocation>
            {
                new GeoItemLocation{Id=0, Name="10511 Larry Way", Address= new Address{Id=0, StreetName="10511 Larry Way", City="Cupertino", State="CA", ZipCode="95014", Latitude=37.330384, Longitude=-122.027499} },
                new GeoItemLocation{Id=0, Name="Bay Prospector", Address= new Address{Id=0, StreetName="20094 Merrit Dr.", City="Cupertino", State="CA", ZipCode="95014", Latitude=37.330384, Longitude=-122.034553} }

            };
        }

        public async Task<bool> AddEvent(GeoEvent geoEvent)
        {
            Events.Add(geoEvent);
            return await Task.FromResult(true);
        }

        public async Task<GeoItemLocation> GetLocationByName(string name)
        {
            return await Task.FromResult(Locations.FirstOrDefault(x => x.Name == name));
        }

        public List<GeoItemLocation> GetTrackedLocations()
        {
            return Locations;
        }
    }
}
