﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GeoDemo.Shared.Models;

namespace GeoDemo.Shared.Services
{
    public interface IGeoTrackerAPI
    {
        Task<bool> AddEvent(GeoEvent geoEvent);

        List<GeoItemLocation> GetTrackedLocations();

        Task<GeoItemLocation> GetLocationByName(string name);

    }
}
