﻿using System;
namespace GeoDemo.Shared.Models
{
    public class GeoItemLocation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Address Address { get; set; }
    }
}
