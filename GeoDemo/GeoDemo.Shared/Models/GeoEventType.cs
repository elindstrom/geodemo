﻿using System;
namespace GeoDemo.Shared.Models
{
    public enum GeoEventType
    {
        Entering,
        Exiting
    }
}
