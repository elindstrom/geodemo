﻿using System;
namespace GeoDemo.Shared.Models
{
    public class GeoEvent
    {
        public int Id { get; set; }
        public GeoEventType EventType { get; set; }
        public int LocationItemId { get; set; }
        public int UserId { get; set; }
    }
}
