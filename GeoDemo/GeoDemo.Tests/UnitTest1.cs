using System.Threading.Tasks;
using GeoDemo.Shared.Models;
using GeoDemo.Shared.Services;
using NUnit.Framework;

namespace GeoDemo.Tests
{
    public class Tests
    {
        GeoTrackerAPI Api;

        [SetUp]
        public void Setup()
        {
            Api = new GeoTrackerAPI();
        }

        [Test]
        public void CanRetrieveLocations()
        {
            var locations = Api.GetTrackedLocations();
            Assert.AreEqual(2, locations.Count);
        }

        [Test]
        public async Task AddEvent_ReturnsTrue()
        {
            var result = await Api.AddEvent(new GeoEvent());
            Assert.IsTrue(result);
        }

        [Test]
        public async Task GetLocationByName_ReturnsItem()
        {
            var result = await Api.GetLocationByName("Bay Prospector");
            Assert.IsNotNull(result);
        }

        [Test]
        public async Task AddEvent_ReturnsNull()
        {
            var result = await Api.GetLocationByName(string.Empty);
            Assert.IsNull(result);
        }
    }
}