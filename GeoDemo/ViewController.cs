﻿using CoreLocation;
using Foundation;
using GeoDemo.Core.Models;
using GeoDemo.Core.Services;
using GeoDemo.Models;
using System;
using System.Threading.Tasks;
using UIKit;

namespace GeoDemo
{
    public partial class ViewController : UIViewController
    {
        LocationManager Manager;
        GeoTrackerAPI TrackerAPI;

        public ViewController(IntPtr handle) : base(handle)
        {
            Manager = new LocationManager();
            Manager.RegionEntered += Manager_RegionEntered;
            Manager.RegionLeft += Manager_RegionLeft;
            Manager.LocationUpdated += WriteLocation;

            TrackerAPI = new GeoTrackerAPI();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            foreach (var location in TrackerAPI.GetTrackedLocations())
            {
                Manager.AddRegionToMonitor(location);
            }

            var successful = Manager.StartLocationUpdates();

            if (!successful)
            {
                ShowAlert("Location services not enabled or App is not authorized to use location data", "Please enable in settings");
                return;
            }

        }

        private void Manager_RegionLeft(object sender, RegionEventArgs e)
        {
            Task.Run(async () =>
            {
                var location = TrackerAPI.GetLocationByName(e.Region.Description);
                await TrackerAPI.AddEvent(new GeoEvent { EventType = GeoEventType.Exiting, UserId = 1, LocationItemId = location.Id });
            });
        }

        private void Manager_RegionEntered(object sender, RegionEventArgs e)
        {
            Task.Run(async () =>
            {
                var location = TrackerAPI.GetLocationByName(e.Region.Description);
                await TrackerAPI.AddEvent(new GeoEvent { EventType = GeoEventType.Entering, UserId = 1, LocationItemId = location.Id });
            });
        }

        public void WriteLocation(object sender, LocationUpdatedEventArgs e)
        {
            CLLocation location = e.Location;
            Console.WriteLine("Longitude: " + location.Coordinate.Longitude);
            Console.WriteLine("Latitude: " + location.Coordinate.Latitude);
        }

        void ShowAlert(string title, string msg)
        {
            var alert = UIAlertController.Create(title, msg, UIAlertControllerStyle.Alert);
            PresentViewController(alert, true, null);
        }
    }
}